#include "parser.h"
#include "syntax_tree/int_variable.h"
#include "syntax_tree/float_variable.h"
#include "syntax_tree/string_variable.h"
#include "syntax_tree/print_action.h"
#include "syntax_tree/expressions/float_expression.h"
#include "syntax_tree/expressions/string_expression.h"
#include "syntax_tree/expressions/identifier_expression.h"
#include "syntax_tree/variable_assignment.h"
#include "syntax_tree/function.h"
#include <iostream>

Parser::Parser (FILE *file)
: file(file)
{
}

void Parser::error (std::string msg) {
	
	std::cout << "Error: line " << linenumber << ", " << msg << std::endl;
	//parsing = false;
	compile = false;
}

void Parser::newlinecheck () {
	
	if (lastchar == '\n') {
		linenumber++;
		lastchar = getnextchar();
		newlinecheck();
	}
}

int Parser::getnextchar () {
	
	int c = getc(file);
//	if (c == '\n') {
//		linenumber++;
//		c = getnextchar();
//	}
	return c;
}

int Parser::gettok () {
	
	newlinecheck();
	
	// skip spaces
	while (isspace(lastchar)) lastchar = getnextchar();
	
	// comments
	if (lastchar == '/') {
		lastchar = getnextchar();
		if (lastchar == '/') {
			int ln = linenumber;
			while (lastchar != EOF && linenumber == ln) {
				lastchar = getnextchar();
				newlinecheck();
			}
			return gettok();
		}
	}
	
	// identifiers or key words
	if (isalpha(lastchar)) {
		identifierstring = lastchar;
		lastchar = getnextchar();
		while (isalnum(lastchar) || lastchar == '_') {
			identifierstring += lastchar;
			lastchar = getnextchar();
		}
		
		if (identifierstring == "int" || identifierstring == "float"
			|| identifierstring == "string" || identifierstring == "bool") {
			return tok_type;
		}
		else if (identifierstring == "print") {
			return tok_action;
		}
		else {
			return tok_identifier;
		}
	}
	
	// numbers
	if (isdigit(lastchar)) {
		std::string number;
		number += lastchar;
		lastchar = getnextchar();
		
		bool pointused = false;
		
		while (isdigit(lastchar) || (lastchar == '.' && !pointused)) {
			if (lastchar == '.') pointused = true;
			number += lastchar;
			lastchar = getnextchar();
		}
		
		numbervalue = strtod(number.c_str(), 0);
		return tok_number;
	}
	
	// string
	if (lastchar == '"') {
		str = "";
		lastchar = getnextchar();
		while (lastchar != '"') {
			str += lastchar;
			lastchar = getnextchar();
		}
		lastchar =  getnextchar();
		return tok_string;
	}
	
	if (lastchar == ';') {
		lastchar = getnextchar();
		return tok_semi_colon;
	}
	
	if (lastchar == EOF) return tok_eof;
	
	lastcharstr = "";
	lastcharstr += lastchar;
	int c = lastchar;
	lastchar = getnextchar();
	return c;
}

void Parser::parse (std::vector<Statement*> &statements, std::map<std::string, int> &statement_map) {
	
	tok = gettok();
	
	// definition
	if (tok == tok_type) {
		std::string type = identifierstring;
		tok = gettok();
		
		if (tok == tok_identifier) {
			std::string identifier = identifierstring;
			tok = gettok();
			
			// variable definition
			if (tok == '=') {
				tok = gettok();
				if (tok == tok_number || tok == tok_string || tok == tok_identifier) {
					Expression *expression = parseExpression(type, statements, statement_map);
					if (expression != nullptr) {
						if (type == "int") {
							IntVariable *i = new IntVariable(identifier, expression);
							statements.push_back(i);
						}
						if (type == "float") {
							FloatVariable *f = new FloatVariable(identifier, expression);
							statements.push_back(f);
						}
						if (type == "string") {
							StringVariable *s = new StringVariable(identifier, expression);
							statements.push_back(s);
						}
						statement_map[identifier] = statements.size()-1;
					}
				}
				else if (tok == tok_semi_colon) {
					error("expected a value after =");
				}
				else {
					error("token after = isn't a value");
				}
			}
			// variable prototype
			else if (tok == tok_semi_colon) {
					if (type == "int") {
						IntVariable *i = new IntVariable(identifier);
						statements.push_back(i);
					}
					if (type == "float") {
						FloatVariable *f = new FloatVariable(identifier);
						statements.push_back(f);
					}
					if (type == "string") {
						StringVariable *s = new StringVariable(identifier);
						statements.push_back(s);
					}
					statement_map[identifier] = statements.size()-1;
			}
			// function
			else if (tok == '(') {
				//list of arguments // need to finish this
				Function *function = new Function(type, identifier);
				
				tok = gettok();
				while (tok != ')') {
					if (tok == tok_type) {
						std::string param_type = identifierstring;
						tok = gettok();
						if (tok == tok_identifier) {
							std::string param_name = identifierstring;
							Parameter *param = new Parameter(param_type, param_name);
							function->parameters.push_back(param);
							function->parameter_map[param_name] = function->parameters.size()-1;
							
							tok = gettok();
							if (tok == ',') {
								tok = gettok();
								if (tok == ')') {
									error("expected another parameter");
									break;
								}
							}
							else if (tok == ')') {
								
							}
							else {
								std::string s = "";
								if (tok < 0) s += "token_id " + std::to_string(tok);
								else s += tok;
								error("IN FUNCTION PARAMETER unknown token, " + s);
							}
						}
						else {
							error("expected a parameter identifier");
							break;
						}
					}
					else {
						error("expected a paramter type");
						break;
					}
				}
				
				tok = gettok();
				if (tok == tok_semi_colon) {
					statements.push_back(function);
					statement_map[identifier] = statements.size()-1;
				}
				else if (tok == '{') {
					// function definition
					function->definition = true;
					in_function = true;
					current_function = function;
					while (in_function) {
						parse(function->statements, function->statement_map);
						if (tok == tok_eof) error("you much close your function at some point");
					}
					
					statements.push_back(function);
					statement_map[identifier] = statements.size()-1;
				}
				else {
					error("missing semi colon or function definition");
				}
			}
			else {
				std::string s = "";
				s += tok;
				std::cout << s << std::endl;
				error("unexpected token after identifier");
			}
		}
		else {
			error("expected identifier after type");
		}
	}
	
	// set or call
	else if (tok == tok_identifier) {
		int index = -1;
		if (statement_map.count(identifierstring)) index = statement_map[identifierstring];
		if (index != -1) {
			std::string type = statements[index]->type;
			tok = gettok();
			// set
			if (tok == '=') {          // Change desc to type then make a set class or something
				tok = gettok();
				Expression *expression = parseExpression(type, statements, statement_map);
				if (expression != nullptr) {
					VariableAssignment *v = new VariableAssignment(identifierstring, expression);
					statements.push_back(v);
				}
			}
			else {
				error("unexpected token after identifier");
			}
		}
		else {
			error("unknown identifier, " + identifierstring);
		}
	}
	
	else if (tok == tok_action) {
		// print
		tok = gettok();
		
		Expression *expression = parseExpression("string", statements, statement_map);
		if (expression != nullptr) {
			PrintAction *p = new PrintAction(expression);
			statements.push_back(p);
		}
		//else {
		//	error("expected value after print");
		//}
//		if (tok == tok_string || tok == tok_number || tok == tok_identifier) {
//			Expression *expression = nullptr;
//			if (tok == tok_string) {
//				expression = new StringExpression(str);
//			}
//			else if (tok == tok_number) {
//				
//			}
//			else {
//				
//			}
//		}
	}
	
	else if (tok == '}' && in_function) {
		in_function = false;
		current_function = nullptr;
	}
	
	else if (tok == tok_eof) parsing = false;
	
	else {
//		std::string s = "";
//		s += tok;
//		s += " ";
//		if (lastchar == '\n') s += "new_line" + s;
//		else s += lastchar;
//		if (tok == tok_number) s += " NUMBER TOKEN";
		std::string s = "";
		if (tok < 0) s += "token_id " + std::to_string(tok);
		else s += tok;
		error("IN PARSE unknown token, " + s);
	}
}

Expression* Parser::parseExpression (std::string type, std::vector<Statement*> &statements, std::map<std::string, int> &statement_map) {
	
	if (type == "int" || type == "float") {
		
		Expression *expression = nullptr; //new FloatExpression(numbervalue);
		if (tok == tok_number) {
			expression = new FloatExpression(numbervalue);
		}
		else if (tok == tok_identifier) {
			// identifier
			std::string identifier_type = statements[statement_map[identifierstring]]->type;
			if (identifier_type != "string") {
				expression = new IdentifierExpression(identifierstring, identifier_type);
			}
			else {
				error("can't do arithmetic on a string");
				return nullptr;
			}
		}
		else {
			error("can't do arithmetic on unknown token");
		}
		
		tok = gettok();
		if (tok == '+' || tok == '-' || tok == '*' || tok == '/') {
			expression->op = lastcharstr;
			tok = gettok();
			expression->rightSide = parseExpression(type, statements, statement_map);
		}
		
		if (tok == tok_semi_colon) return expression;
		
		else {
			std::string s = "";
			if (tok < 0) s += "token_id " + std::to_string(tok);
			else s += tok;
			error("unknown token, " + s);
		}
	}
	
	if (type == "string") {
		
		Expression *expression = nullptr;
		if (tok == tok_string) {
			expression = new StringExpression(str);
		}
		else if (tok == tok_number) {
			expression = new FloatExpression(numbervalue);
		}
		else if (tok == tok_identifier) {
			// identifier
			if (statement_map.count(identifierstring)) {
				std::string identifier_type = statements[statement_map[identifierstring]]->type;
				expression = new IdentifierExpression(identifierstring, identifier_type);
			}
			else if (current_function != nullptr && current_function->parameter_map.count(identifierstring)) {
				std::string identifier_type = current_function->parameters[current_function->parameter_map[identifierstring]]->type;
				expression = new IdentifierExpression(identifierstring, identifier_type);
			}
			else {
				error("unknown identifier, " + identifierstring);
				return nullptr;
			}
		}
		else {
			error("can't add unknown token to string");
			return nullptr;
		}
		
		expression->tostring = true;
		
		tok = gettok();
		if (tok == '+') {
			expression->op = lastcharstr;
			tok = gettok();
			expression->rightSide = parseExpression(type, statements, statement_map);
		}
		
		if (tok == tok_semi_colon) return expression;
		
		else {
			std::string s = "";
			if (tok < 0) s += "token_id " + std::to_string(tok);
			else s += tok;
			error("IN STRING EXPRTESSION unknown token, " + s);
		}
	}
	
	return nullptr;
}
