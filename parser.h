#ifndef __lang__parser__
#define __lang__parser__

#include "syntax_tree/statement.h"
#include "expression.h"
#include "function.h"
#include <vector>
#include <map>

class Parser {
	
	enum Tok {
		tok_eof = -1,
		tok_identifier = -2,
		tok_type = -3,
		tok_number = -4,
		tok_action = -5,
		tok_string = -6,
		tok_semi_colon = -7
	};
	
	FILE *file;
	int lastchar = ' ';
	std::string lastcharstr = "";
	std::string identifierstring;
	std::string str;
	float numbervalue;
	int linenumber = 1;
	int tok = 0;
	bool in_function = false;
	Function *current_function = nullptr;
	
public:
	Parser (FILE *file);
	int getnextchar ();
	int gettok ();
	void parse (std::vector<Statement*> &statements, std::map<std::string, int> &statement_map);
	Expression* parseExpression (std::string type, std::vector<Statement*> &statements, std::map<std::string, int> &statement_map);
	void error (std::string msg);
	void newlinecheck ();
	
	bool parsing = true;
	bool compile = true;
	std::vector<Statement*> main_statements;
	std::map<std::string, int> main_statement_map;
	
};

#endif