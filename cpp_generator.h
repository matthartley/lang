#ifndef __lang__cpp_generator__
#define __lang__cpp_generator__

#include "parser.h"
#include <iostream>
#include <fstream>

class CppGenerator {
	
public:
	std::ofstream output;
	CppGenerator ();
	//~CppGenerator ();
	void compile ();
	void gen (Parser parser);
	
};

#endif