#include "cpp_generator.h"

CppGenerator::CppGenerator () {
	
	output.open("output.cpp");
}

void CppGenerator::gen (Parser parser) {
	
	output << "#include <iostream>" << std::endl;
	output << "int main () {" << std::endl;
	
	for (int i = 0; i < parser.main_statements.size(); i++) {
		output << parser.main_statements[i]->codegen();
	}
	
	output << "}" << std::endl;
}

//CppGenerator::~CppGenerator () {
//	
//	//output.close();
//}

/*void CppGenerator::genVariable (std::string name, std::string type, std::string value) {
	
	if (value != "") {
		output << "	" << type << " " << name << " = " << value << ";" << std::endl;
	}
	else {
		output << "	" << type << " " << name << ";" << std::endl;
	}
}*/