#include "identifier_expression.h"

IdentifierExpression::IdentifierExpression (std::string value, std::string type)
: Expression(type)
, value(value)
{
	
}

std::string IdentifierExpression::codegen () {
	
	return (type != "string" && tostring ? "std::to_string(" + value + ")" : value) + (op != "" ? " " + op + " " + rightSide->codegen() : "");
}