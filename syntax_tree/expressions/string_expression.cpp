#include "string_expression.h"

StringExpression::StringExpression (std::string value)
: Expression("string")
, value(value)
{
	
}

std::string StringExpression::codegen () {
	
	if(rightSide != nullptr) rightSide->tostring = true;
	return "\"" + value + "\"" + (op != "" ? " " + op + " " + rightSide->codegen() : "");
}