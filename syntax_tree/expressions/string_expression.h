#ifndef __lang__string_expression__
#define __lang__string_expression__

#include "expression.h"
#include <iostream>

class StringExpression : public Expression {
	
public:
	std::string value;
	StringExpression (std::string value);
	std::string codegen ();
	
};

#endif