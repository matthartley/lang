#ifndef __lang__identifier_expression__
#define __lang__identifier_expression__

#include "expression.h"
#include <iostream>

class IdentifierExpression : public Expression {
	
public:
	std::string value;
	IdentifierExpression (std::string value, std::string type);
	std::string codegen ();
	
};

#endif