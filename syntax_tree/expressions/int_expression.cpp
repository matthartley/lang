#include "int_expression.h"

IntExpression::IntExpression (int value)
: Expression("int")
, value(value)
{
}

std::string IntExpression::codegen () {
	
	return std::to_string(value) + (op != "" ? " " + op + " " + rightSide->codegen() : "");
}