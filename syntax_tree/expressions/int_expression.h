#ifndef __lang__int_expression__
#define __lang__int_expression__

#include "expression.h"
#include <iostream>

class IntExpression : public Expression {
	
public:
	int value;
	IntExpression (int value);
	std::string codegen ();
	
};

#endif