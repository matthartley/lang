#ifndef __lang__expression__
#define __lang__expression__

#include <iostream>

class Expression {
	
public:
	std::string op = "";
	Expression *rightSide;
	bool brackets = false;
	bool tostring = false;
	
	std::string type;
	Expression (std::string type);
	virtual std::string codegen ();
	
};

#endif
