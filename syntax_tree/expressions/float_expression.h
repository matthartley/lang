#ifndef __lang__float_expression__
#define __lang__float_expression__

#include "expression.h"
#include <iostream>

class FloatExpression : public Expression {
	
public:
	float value;
	FloatExpression (float value);
	std::string codegen ();
	
};

#endif