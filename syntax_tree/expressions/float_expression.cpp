#include "float_expression.h"

FloatExpression::FloatExpression (float value)
: Expression("float")
, value(value)
{
}

std::string FloatExpression::codegen () {
	
//	char c = static_cast<char>(op);
//	std::cout << "float expression code gen" << std::endl;
	return (tostring ? "std::to_string(" + std::to_string(value) + ")" : std::to_string(value)) + (op != "" ? " " + op + " " + rightSide->codegen() : "");
}