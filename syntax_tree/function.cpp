#include "function.h"

Function::Function (std::string type, std::string name)
: Statement(type)
, name(name)
{
	
}

std::string Function::codegen () {
	
	std::string output = "	" + type + " " + name + "(";
	for (int i = 0; i < parameters.size(); i++) {
		output += parameters[i]->type + " " + parameters[i]->name;
		if (i < parameters.size()-1) output += ", ";
	}
	output += ")";
	
	if (definition) {
		output += "{\n";
		for (int i = 0; i < statements.size(); i++) {
			output += "	" + statements[i]->codegen();
		}
		output += "}\n";
	}
	else {
		output += ";\n";
	}
	
	return output;
}