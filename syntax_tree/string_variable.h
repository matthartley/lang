#ifndef __lang__string_variable__
#define __lang__string_variable__

#include "statement.h"
#include "expression.h"

class StringVariable : public Statement {
	
public:
	std::string name;
	Expression *value;
	
	StringVariable (std::string name);
	StringVariable (std::string name, Expression *value);
	std::string codegen ();
	
};

#endif