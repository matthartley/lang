#ifndef __lang__print_action__
#define __lang__print_action__

#include "statement.h"
#include "expression.h"
#include <iostream>

class PrintAction : public Statement {
	
public:
	Expression *str;
	PrintAction (Expression *str);
	std::string codegen ();
	
};

#endif