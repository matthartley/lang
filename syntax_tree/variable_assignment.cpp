#include "variable_assignment.h"

VariableAssignment::VariableAssignment (std::string name, Expression *value)
: Statement("assign")
, name(name)
, value(value)
{
	
}

std::string VariableAssignment::codegen () {
	
	return "	" + name + " = " + value->codegen() + ";\n";
}