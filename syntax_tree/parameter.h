#ifndef __lang__parameter__
#define __lang__parameter__

#include <iostream>

class Parameter {
	
public:
	std::string type;
	std::string name;
	
	Parameter (std::string type, std::string name);
	
};

#endif