#include "print_action.h"

PrintAction::PrintAction (Expression *str)
: Statement("print")
, str(str)
{
}

std::string PrintAction::codegen () {
	
	return "	std::cout << " + str->codegen() + " << std::endl;\n";
}