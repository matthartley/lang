#ifndef __lang__int_variable__
#define __lang__int_variable__

#include "statement.h"
#include "expression.h"

class IntVariable : public Statement {
	
public:
	std::string name;
	Expression *value;
	
	IntVariable (std::string name);
	IntVariable (std::string name, Expression *value);
	std::string codegen ();
	
};

#endif