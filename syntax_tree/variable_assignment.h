#ifndef __lang__variable_assignment__
#define __lang__variable_assignment__

#include "statement.h"
#include "Expression.h"
#include <iostream>

class VariableAssignment : public Statement {
	
public:
	std::string name;
	Expression *value;
	
	VariableAssignment (std::string name, Expression *value);
	std::string codegen ();
	
};

#endif