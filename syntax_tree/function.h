#ifndef __lang__function__
#define __lang__function__

#include "statement.h"
#include "parameter.h"
#include <iostream>
#include <vector>
#include <map>

class Function : public Statement {
	
public:
	std::string name;
	std::vector<Parameter*> parameters;
	std::vector<Statement*> statements;
	std::map<std::string, int> parameter_map;
	std::map<std::string, int> statement_map;
	bool definition = false;
	
	Function (std::string type, std::string name);
	std::string codegen ();
	
};

#endif