#include "string_variable.h"

StringVariable::StringVariable (std::string name)
: Statement("string")
, name(name)
{
}

StringVariable::StringVariable (std::string name, Expression *value)
: Statement("string")
, name(name)
, value(value)
{
}

std::string StringVariable::codegen () {
	
	if (value != nullptr) {
		return "	std::string " + name + " = " + value->codegen() + ";\n";
	}
	else {
		return "	std::string " + name + ";\n";
	}
}