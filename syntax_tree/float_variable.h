#ifndef __lang__float_variable__
#define __lang__float_variable__

#include "statement.h"
#include "expression.h"

class FloatVariable : public Statement {
	
public:
	std::string name;
	Expression *value;
	
	FloatVariable (std::string name);
	FloatVariable (std::string name, Expression *value);
	std::string codegen ();
	
};

#endif
