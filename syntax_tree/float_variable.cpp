#include "float_variable.h"

FloatVariable::FloatVariable (std::string name)
: Statement("float")
, name(name)
{
}

FloatVariable::FloatVariable (std::string name, Expression *value)
: Statement("float")
, name(name)
, value(value)
{
}

std::string FloatVariable::codegen () {
	
	if (value != nullptr) {
		return "	float " + name + " = " + value->codegen() + ";\n";
	}
	else {
		return "	float " + name + ";\n";
	}
}