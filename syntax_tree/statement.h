#ifndef __lang__statement__
#define __lang__statement__

#include <iostream>

class Statement {
	
public:
	std::string type;
	Statement (std::string type);
	virtual std::string codegen ();
	
};

#endif
