#include "int_variable.h"

IntVariable::IntVariable (std::string name)
: Statement("int")
, name(name)
{
}

IntVariable::IntVariable (std::string name, Expression *value)
: Statement("int")
, name(name)
, value(value)
{
}

std::string IntVariable::codegen () {
	
	if (value != nullptr) {
		return "	int " + name + " = " + value->codegen() + ";\n";
	}
	else {
		return "	int " + name + ";\n";
	}
}