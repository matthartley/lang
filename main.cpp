// folder syntax_tree with int_variable, float_variable, int_function etc
// create a bitbucket repo
// Definition for variables, functions etc; Expression for binary operations (1 + (2 * 3))

#include "parser.h"
#include "cpp_generator.h"
#include "syntax_tree/int_variable.h"

#include <iostream>
#include <fstream>
#include <stdio.h>

std::string filepath = "main.jel";//"/Users/Matt/desktop/lang/main.jel";

int main (int argc, const char * argv[]) {
	
	if (argc >= 2) {
		filepath = argv[1];
	}
	
	FILE *file = fopen(filepath.c_str(), "r");
	if (file == nullptr) {
		std::cout << "Can't open file " << filepath << std::endl;
		return 1;
	}
	
	Parser parser = Parser(file);
	while (parser.parsing) {
		parser.parse(parser.main_statements, parser.main_statement_map);
	}
	
	if (parser.compile) {
		CppGenerator cppgen = CppGenerator();
		cppgen.gen(parser);
	
		std::cout << "compiling..." << std::endl;
		int compilecode = system("clang++ -g -O3 output.cpp -o output");
		std::cout << "compile code " << compilecode << (compilecode==0 ? ", success" : ", failed") << std::endl;
	}
	
	fclose(file);
    return 0;
}